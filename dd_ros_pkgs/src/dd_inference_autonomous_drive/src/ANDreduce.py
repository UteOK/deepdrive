import Logic
from ORreduce import ORreduce
from dd_inference_autonomous_drive.msg import dd_obj_msg


class ANDreduce(Logic.Logic):
    def __init__(self, frames_start, frames_end, objectname):
        self.frames_start = frames_start
        self.frames_end = frames_end
        self.objectname = objectname

    def __call__(self):
        if self.frames_end != 0:
            l = self.framelist[-self.frames_start:-self.frames_end]
        else:
            l = self.framelist[-self.frames_start:]
        boo = True
        if self.objectname == "aqua":
            for f in l:
                boo = boo and f.aqua
        elif self.objectname == "stop":
            for f in l:
                boo = boo and f.stop
        elif self.objectname == "person":
            for f in l:
                p = f.person
                boo = boo and p
        elif self.objectname == "thirty_end":
            for f in l:
                boo = boo and f.thirty_end
        elif self.objectname == "thirty_start":
            for f in l:
                boo = boo and f.thirty_start

        return boo

if __name__ == "__main__":
    boxes1 = []
    boxes1.append(dd_obj_msg(xmin=0, xmax=400, ymin=0, ymax=480,
                             class_=str(2), probability=0.8))
    boxes0 = []
    boxes0.append(dd_obj_msg(xmin=42, xmax=42, ymin=0, ymax=480,
                             class_=str(3), probability=0.8))

    Logic.Logic.fillList(boxes1)
    Logic.Logic.fillList(boxes0)
    for i in range(5):
        Logic.Logic.fillList(boxes1)

    a = ANDreduce(4, 0, "aqua")
    Logic.Logic.printList()

    print(a())
    quit()

    for i in range(4):
        Logic.Logic.fillList(boxes0)
    test1 = ANDreduce(4, 0, "person")
    test2 = ORreduce(25, 0, "person_executed")
    gesamt1=test1() and not(test2())
    if gesamt1==True:
        e=Logic.Logic.lastFrame()
        e.person_executed=True
    #Logic.lastFrame().person_executed = test1
    print('test1: ', test1())
    print('test2: ', test2())
    print('gesamt1: ', gesamt1)
    print('Erwartung: ', 'True')

    for i in range(25):
        Logic.Logic.fillList(boxes1)

    test1= ANDreduce(4, 0, "person")
    test2= ORreduce(25, 0, "person_executed")
    gesamt1=test1() and not(test2())
    Logic.Logic.lastFrame().person_executed = gesamt1
    print('test1: ', test1())
    print('test2: ', test2())
    print('gesamt1: ', gesamt1)
    print('Erwartung: ', 'False')

    for i in range(4):
        Logic.Logic.fillList(boxes0)
    test1= ANDreduce(4, 0, "person")
    test2= ORreduce(25, 0, "person_executed")
    gesamt1=test1() and not(test2())
    Logic.Logic.lastFrame().person_executed = gesamt1

    print('test1: ', test1())
    print('test2: ', test2())
    print('gesamt1: ', gesamt1)
    print('Erwartung: ', 'True')

    for i in range(4):
        Logic.Logic.fillList(boxes0)

    test1= ANDreduce(4, 0, "person")
    test2= ORreduce(25, 0, "person_executed")
    gesamt1=test1() and not(test2())
    Logic.Logic.lastFrame().person_executed = gesamt1

    print('test1: ', test1())
    print('test2: ', test2())
    print('gesamt1: ', gesamt1)
    print('Erwartung: ', 'False')


    for i in range(4):
        Logic.Logic.fillList(boxes0)
    for i in range(1):
        Logic.Logic.fillList(boxes1)
    for i in range(4):
        Logic.Logic.fillList(boxes0)

    test1= ANDreduce(4, 0, "person")
    test2= ORreduce(25, 0, "person_executed")
    gesamt1=test1() and not(test2())
    Logic.Logic.lastFrame().person_executed = gesamt1

    print('test1: ', test1())
    print('test2: ', test2())
    print('gesamt1: ', gesamt1)
    print('Erwartung: ', 'False')


