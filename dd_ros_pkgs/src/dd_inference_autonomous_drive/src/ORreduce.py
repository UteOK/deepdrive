import Logic
from dd_inference_autonomous_drive.msg import dd_obj_msg


class ORreduce(Logic.Logic):
    def __init__(self, frames_start,frames_end, objectname):
        self.frames_start = frames_start
        self.frames_end = frames_end
        self.objectname = objectname

    def __call__(self):
        if self.frames_end != 0:
            l = self.framelist[-self.frames_start:-self.frames_end]
        else:
            l = self.framelist[-self.frames_start:]
        boo = False
        for f in l:  # loop over frames
            if f.aqua and self.objectname == "aqua":
                boo = True
            elif f.stop and self.objectname == "stop":
                boo = True
            elif f.person and self.objectname == "person":
                boo = True
            elif f.thirty_end and self.objectname == "thirty_end":
                boo = True
            elif f.thirty_start and self.objectname == "thirty_start":
                boo = True
            elif f.aqua_executed and self.objectname == "aqua_executed":
                boo = True
            elif f.stop_executed and self.objectname == "stop_executed":
                boo = True
            elif f.person_out and self.objectname == "person_out":
                boo = True
            elif f.thirty_end_executed and self.objectname == "thirty_end_executed":
                boo = True
            elif f.thirty_start_executed and self.objectname == "thirty_start_executed":
                boo = True

        return boo


if __name__ == "__main__":
    boxes0 = []
    boxes0.append(dd_obj_msg(xmin=100, xmax=200, ymin=0, ymax=480,
                             class_=str(5), probability=0.8))
    boxes0.append(dd_obj_msg(xmin=500, xmax=650, ymin=200, ymax=300,
                             class_=str(3), probability=0.8))
    boxes1 = []
    boxes1.append(dd_obj_msg(xmin=42, xmax=42, ymin=0, ymax=480,
                             class_=str(6), probability=0.8))

    Logic.fillList(boxes0)
    Logic.fillList(boxes1)
    Logic.fillList(boxes1)
    Logic.fillList(boxes1)
    Logic.fillList(boxes1)

    for i in Logic.framelist:
        print(i)

    a1 = ORreduce(5,0, "person")
    print(a1())
