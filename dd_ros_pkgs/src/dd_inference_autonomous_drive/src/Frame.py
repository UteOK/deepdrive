from dd_inference_autonomous_drive.msg import dd_obj_msg
from ORreduce import ORreduce
from ANDreduce import ANDreduce


class Frame:
    class_names = ["aqua", "aqua_executed", "stop", "stop_executed", "person", "person_out",  "thirty_start",
                   "thirty_start_executed", "thirty_end", "thirty_end_executed"]

    def __init__(self, boxes):
        self.boxes = boxes

        # Detected
        self.aqua = False
        self.stop = False
        self.thirty_start = False
        self.thirty_end = False
        self.person = False

        # Executed
        self.aqua_executed = False
        self.stop_executed = False
        self.thirty_start_executed = False
        self.thirty_end_executed = False
        self.person_out = True

        # Spatial Or Reduce (1 erkannte Box reicht)
        for box in boxes:
            if self._aqua_condition_single_frame(box):
                self.aqua = True
            if self._stop_condition_single_frame(box):
                self.stop = True
            if self._thirty_start_condition_single_frame(box):
                self.thirty_start = True
            if self._thirty_end_condition_single_frame(box):
                self.thirty_end = True
            if self._person_condition_single_frame(box):
                self.person = True

        self.aqua_executed = self._aqua_condition_temporal()
        self.stop_executed = self._stop_condition_temporal()
        self.thirty_start_executed = self._thirty_start_condition_temporal()
        self.thirty_end_executed = self._thirty_end_condition_temporal()
        self.person_out = self._person_out()

    def _stop_condition_single_frame(self, box):
        centerx, centery, height_box, width_box = self._rect_to_center_trans(box)
        spacial_cond = centerx >= 0.4 and height_box >= 0.1 and box.class_ == '1'
        return spacial_cond

    def _stop_condition_temporal(self):
        anr = ANDreduce(2, 0, "stop")
        orr = ORreduce(40, 0, "stop_executed")
        temporal_cond = not orr() and anr()
        return temporal_cond

    def _aqua_condition_single_frame(self, box):
        centerx, centery, height_box, width_box = self._rect_to_center_trans(box)
        spacial_cond = width_box >= 0.35 and box.class_ == '2'
        return spacial_cond

    def _aqua_condition_temporal(self):
        anr = ANDreduce(3, 0, "aqua")
        orr = ORreduce(25, 0, "aqua_executed")
        temporal_cond = anr() and not orr()
        return temporal_cond

    def _thirty_start_condition_single_frame(self, box):
        centerx, centery, height_box, width_box = self._rect_to_center_trans(box)
        spacial_cond = centerx >= 0.5 and height_box >= 0.1 and box.class_ == '3'
        return spacial_cond

    def _thirty_start_condition_temporal(self):
        anr = ANDreduce(2, 0, "thirty_start")
        orr = ORreduce(10, 0, "thirty_end_executed")
        return anr() and not orr()

    def _thirty_end_condition_single_frame(self, box):
        centerx, centery, height_box, width_box = self._rect_to_center_trans(box)
        spacial_cond = centerx >= 0.4 and height_box >= 0.09 and box.class_ == '4'
        return spacial_cond

    def _thirty_end_condition_temporal(self):
        return self.thirty_end

    def _person_condition_single_frame(self, box):
        centerx, centery, height_box, width_box = self._rect_to_center_trans(box)
        return centerx>=0.15 and centerx<=0.85 and height_box >= 0.23 and box.class_ == '5'

    def _person_out(self):
        p = ORreduce(10, 0, 'person')
        return not p()


    def _rect_to_center_trans(self,box, width=640, height=480):
        centerx=float((box.xmax+box.xmin))/2/width
        centery=float((box.ymax+box.ymin))/2/height
        height_box=float((box.ymax-box.ymin))/height
        width_box=float((box.xmax-box.xmin))/width
        return centerx, centery, height_box, width_box

    def __str__(self):
        s  = 'aqua:                  ' + str(self.aqua)+'\n'
        s += 'stop:                  ' + str(self.stop) +'\n'
        s += 'person:                ' + str(self.person)+'\n'
        s += 'thirty_end:            ' + str(self.thirty_end)+'\n'
        s += 'thirty_start:          ' + str(self.thirty_start)+'\n'
        s += 'aqua_executed:         ' + str(self.aqua_executed) + '\n'
        s += 'stop_executed:         ' + str(self.stop_executed) + '\n'
        s += 'person_executed:       ' + str(self.person_executed)+'\n'
        s += 'thirty_end_executed:   ' + str(self.thirty_end_executed)+'\n'
        s += 'thirty_start_executed: ' + str(self.thirty_start_executed)+'\n\n'

        return s

    def getFlagVector(self):
        s = []
        for i in self.class_names:
            s.append(getattr(self, i))
        return s


if __name__ == "__main__":
    boxes=[]
    testbox=[50,150,20,120,"5",0.7]
    boxes.append(dd_obj_msg(xmin=100, xmax=200, ymin=0, ymax=480,
                                     class_=str(5), probability=0.8))
    boxes.append(dd_obj_msg(xmin=500, xmax=650, ymin=200, ymax=300,
                                     class_=str(3), probability=0.8))
    p1=Frame(boxes)
    print(p1)
