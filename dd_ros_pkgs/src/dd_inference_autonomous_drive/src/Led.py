import time
import threading
import os


if os.uname().machine == "armv7l":
    import RPi.GPIO as GPIO

class Led:
    def __init__(self, gpio_number, period=1, duty_cycle=0.5):
        if os.uname().machine == "armv7l":
            self.gpio_number = gpio_number
            self.period = period
            self.duty_cycle = duty_cycle
            self._setup()
            self._state = False
            self._raspi4=True
            self.off()
        else:
            self._raspi4=False

    def _setup(self):
        GPIO.setmode(GPIO.BCM)
        GPIO.setwarnings(False)
        GPIO.setup(self.gpio_number, GPIO.OUT)

    def blink(self, times=1):
        for i in range(times):
            t = threading.Thread(target=self._blink())
            t.start()

    def _blink(self):
        self.on()
        #time.sleep(self.period*self.duty_cycle)
        self.off()

    def on(self):
        self._state = True
        if self._raspi4:
            GPIO.output(self.gpio_number, GPIO.HIGH)
        else:
            print("LED an")

    def off(self):
        self._state = False
        if self._raspi4:
            GPIO.output(self.gpio_number, GPIO.LOW)
        else:
            print("LED an")

    def toggle(self):
        if self._state:
            self.off()
        else:
            self.on()


if __name__ == "__main__":
    l1 = Led(23)
    l1.blink()
