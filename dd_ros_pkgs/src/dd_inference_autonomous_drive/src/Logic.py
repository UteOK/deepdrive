from dd_inference_autonomous_drive.msg import dd_obj_msg


class Logic:
    #  Liste von Frameobjekten
    framelist = []
    framelist_size = 40

    @classmethod
    def fillList(cls, boxes):
        import Frame  # Circular dependency
        f = Frame.Frame(boxes)
        if len(cls.framelist)>=cls.framelist_size:
            cls.framelist.pop(0)
        cls.framelist.append(f)

    @classmethod
    def lastFrame(cls):
        return cls.framelist[-1]

    @classmethod
    def printList(cls, last=10):
        import Frame
        import tabulate
        table = []
        for n, f in enumerate(cls.framelist):
            table.append([n] + f.getFlagVector())
        table = table[-last:]
        header = ["#"] + Frame.Frame.class_names
        header = [h[:10] for h in header] # nur erste 10 zeichen
        print(tabulate.tabulate(table, headers=header))


if __name__ == "__main__":
    boxes = []
    boxes.append(dd_obj_msg(xmin=99, xmax=200, ymin=0, ymax=480,
                            class_=str(6), probability=0.8))
    boxes.append(dd_obj_msg(xmin=500, xmax=650, ymin=200, ymax=300,
                            class_=str(6), probability=0.8))

    Logic.framelist_size = 3
    Logic.fillList(boxes)
    Logic.fillList(boxes)
    Logic.fillList(boxes)
    Logic.fillList(boxes)
    boxes = []
    boxes.append(dd_obj_msg(xmin=42, xmax=42, ymin=0, ymax=480,
                            class_=str(6), probability=0.8))
    Logic.fillList(boxes)

    for i in Logic.framelist:
        print(i.boxes)