#!/usr/bin/env python

# Source
# * From example in https://www.hackster.io/news/benchmarking-tensorflow-and-tensorflow-lite-on-the-raspberry-pi-43f51b796796
# * object_detection_tutorial.ipynb

import numpy as np
import tensorflow as tf
import time
import os
import rospy
import cv2
from sensor_msgs.msg import CompressedImage
from sensor_msgs.msg import Joy
from std_msgs.msg import Float64
from dd_inference_autonomous_drive.msg import dd_obj_msg
from dd_inference_autonomous_drive.msg import dd_frame_predicted_msg
import time
from enum import Enum
if os.uname().machine == "armv7l":
    from Led import Led
else:
    from Led import Led

from ORreduce import ORreduce
from Logic import Logic

forward_vector = [0.0, 0.2, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
backward_vector = [0.0, -0.2, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
right_vector = [0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0]
left_vector = [0.0, 0.0, 0.0, -1.0, 0.0, 0.0, 0.0, 0.0]
stop_vector = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]

buttons_vector =   [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
start_lanefollow = [0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0]
stop_lanefollow =  [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0]

start_slow_lanefollow = [0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0]
stop_slow_lanefollow =  [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0]

velocity_thirty_zone = 0.8 # 0.7
velocity_default = 1.8 # 1.0


class dd_enum(Enum):
    stopp = 1
    aqua = 2
    drive_30 = 3
    thirty_end = 4
    person = 5
    stopp_30 = 6
    aqua_30 = 7
    person_30 = 8
    drive = 9


class dd_inference_autonomous_drive_node:
    def __init__(self, name, image_size=640*480*3):
        # Debug LEDS
        self.blue_led = Led(23)
        self.red_led = Led(17)
        self.orange_led = Led(27)
        self.yellow_led = Led(22)

        # Load TF Model
        self.detection_graph = tf.Graph()
        with self.detection_graph.as_default():
            od_graph_def = tf.GraphDef()
            #with tf.gfile.GFile("../../../../dd_objdet/model/frozen_inference_graph_20191228.pb", "rb") as tf_file:
            with tf.gfile.GFile(os.environ["DD_MODEL_PATH"], "rb") as tf_file:

                serialized_graph = tf_file.read()
                od_graph_def.ParseFromString(serialized_graph)
                tf.import_graph_def(od_graph_def, name='')
        self.tf_session = tf.Session(graph=self.detection_graph)
        self.tf_session.__enter__()

        self.state = dd_enum.drive
        
        self.name = name
        rospy.init_node("object_detection", anonymous=True)
        
        # Erklareung fuer buff_size in Subsriber:
        # https://answers.ros.org/question/220502/image-subscriber-lag-despite-queue-1/
        self.buffer_size = int(3 * 10**6)  # Ermittelt durch testen
        # Subscibe to Duckie compressed image
        self.image_sub = rospy.Subscriber("/" + self.name + "/camera_node/image/compressed",
                                          CompressedImage, self.object_detection,  queue_size=1,
                                          buff_size=self.buffer_size)
        # Publish compressed image with boxes
        self.bbox_pub = rospy.Publisher("/" + name + "/object_detection/compressed_image_with_boxes",
                                        dd_frame_predicted_msg, queue_size=1)
        
        # Publisher fuer Joystick
        self.joy_pub = rospy.Publisher("/" + name + "/joy", Joy, queue_size=1)
        self.count = 0

        # Publisher fuer 30 Zone
        self.velo_pub = rospy.Publisher("/" + name + "/thirty_zone", Float64, queue_size=1)

        self.start_lanefollow()
        # Default velocity
        self.velo_pub.publish(Float64(velocity_default))

    def __del__(self):
        self.tf_session.close()


    def start_lanefollow(self):
        time.sleep(0.1)
        for k in range(3):
            time.sleep(0.1)
            msg = Joy(header=None, axes=stop_vector, buttons=start_lanefollow)
            self.joy_pub.publish(msg)
        time.sleep(0.1)
        msg = Joy(header=None, axes=stop_vector, buttons=buttons_vector)
        self.joy_pub.publish(msg)
        time.sleep(0.1)

    def slow_lanefollow(self):
        time.sleep(0.1)
        for k in range(3):
            time.sleep(0.1)
            msg = Joy(header=None, axes=stop_vector, buttons=start_slow_lanefollow)
            self.joy_pub.publish(msg)
        time.sleep(0.1)
        msg = Joy(header=None, axes=stop_vector, buttons=buttons_vector)
        self.joy_pub.publish(msg)
        time.sleep(0.1)

    def stop_lanefollow(self):
        time.sleep(0.1)
        for k in range(3):
            time.sleep(0.1)
            msg = Joy(header=None, axes=stop_vector, buttons=stop_lanefollow)
            self.joy_pub.publish(msg)
        time.sleep(0.1)
        msg = Joy(header=None, axes=stop_vector, buttons=buttons_vector)
        self.joy_pub.publish(msg)
        time.sleep(0.1)

    def stop_slow_lanefollow(self):
        time.sleep(0.1)
        for k in range(3):
            time.sleep(0.1)
            msg = Joy(header=None, axes=stop_vector, buttons=stop_slow_lanefollow)
            self.joy_pub.publish(msg)
        time.sleep(0.1)
        msg = Joy(header=None, axes=stop_vector, buttons=buttons_vector)
        self.joy_pub.publish(msg)
        time.sleep(0.1)

    def rotation(self):
        time.sleep(0.1)
        for i in range(2):
            msg = Joy(header=None, axes=right_vector, buttons=buttons_vector)
            self.joy_pub.publish(msg)
            time.sleep(0.1)
            msg = Joy(header=None, axes=left_vector, buttons=buttons_vector)
            self.joy_pub.publish(msg)
            time.sleep(0.1)

    def back_and_forth(self):
        time.sleep(0.1)
        for i in range(2):
            msg = Joy(header=None, axes=backward_vector, buttons=buttons_vector)
            self.joy_pub.publish(msg)
            time.sleep(0.6)
            msg = Joy(header=None, axes=forward_vector, buttons=buttons_vector)
            self.joy_pub.publish(msg)
            time.sleep(0.6)


    def object_detection(self, ros_image):
        self.blue_led.toggle()
        msg = dd_frame_predicted_msg()
        msg.frame.header.frame_id = "55435"
        msg.frame = ros_image

        # Boxen predicten
        image = ros_image.data
        image = np.fromstring(image, np.uint8)
        image = cv2.imdecode(image, cv2.IMREAD_COLOR)
        msg.boxes = self.predict(image, model="/home/ute/Downloads/frozen_inference_graph2.pb", model_type="pb")

        # Kommando an Duckiebot
        self.decision_logic(msg.boxes)

        rospy.loginfo(len(ros_image.data))
        self.bbox_pub.publish(msg)

    def predictlite(self, image, model):
        tf_interpreter = tf.lite.Interpreter(model_path=model)
        tf_interpreter.allocate_tensors()

        input_details = tf_interpreter.get_input_details()
        output_details = tf_interpreter.get_output_details()
        height = input_details[0]['shape'][1]
        width = input_details[0]['shape'][2]

        tf_interpreter.set_tensor(input_details[0]['index'], input_data)

        tf_interpreter.invoke()
        detected_boxes = tf_interpreter.get_tensor(output_details[0]['index'])
        detected_classes = tf_interpreter.get_tensor(output_details[1]['index'])
        detected_scores = tf_interpreter.get_tensor(output_details[2]['index'])
        num_boxes = tf_interpreter.get_tensor(output_details[3]['index'])

        return detected_boxes, detected_scores, detected_classes, num_boxes

    def predictnormal(self, input_data):
        image_tensor = self.detection_graph.get_tensor_by_name('image_tensor:0')
        detection_boxes = self.detection_graph.get_tensor_by_name('detection_boxes:0')
        detection_scores = self.detection_graph.get_tensor_by_name('detection_scores:0')
        detection_classes = self.detection_graph.get_tensor_by_name('detection_classes:0')
        num_detections = self.detection_graph.get_tensor_by_name('num_detections:0')
        (boxes, scores, classes, num) = self.tf_session.run([detection_boxes,
                                                             detection_scores,
                                                             detection_classes,
                                                             num_detections],
                                                            feed_dict={image_tensor: input_data})
        return boxes, scores, classes, num

    def predict(self, image, model=None, model_type="tflite"):
        print("Using model %s %s" % (model, model_type) )
        # BGR to RGB
        image_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

        boxes_detected = []

        initial_h, initial_w, channels = image.shape
        width, height = 300, 300
        frame = cv2.resize(image_rgb, (width, height))
        input_data = np.expand_dims(frame, axis=0)

        inference_start = time.time()

        if model_type == "tflite":
            boxes, scores, classes, num = self.predictlite(image, model)
        elif model_type == "pb":
            boxes, scores, classes, num = self.predictnormal(input_data)
        else:
            raise ValueError("Falscher Wert. Nur 'pb' und tflite relaubt ")

        inference_end = time.time()
        inference_duration = inference_end - inference_start
        print("Inference time " + str(inference_duration))

        for i in range(int(num)):
            box = np.array(boxes[0][i])
            box[box < 0] = 0 
            bottom, left, top, right = box
            class_id = int(classes[0][i])
            score = scores[0][i]
            if score > 0.5:
                xmin = int(left * initial_w)
                ymin = int(bottom * initial_h)
                xmax = int(right * initial_w)
                ymax = int(top * initial_h)
                boxes_detected.append(dd_obj_msg(xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax,
                                                 class_=str(class_id), probability=score))

        print(boxes_detected)
        return boxes_detected

    def decision_logic(self, boxes):

        print("State: ",self.state)
        print("Count: ", self.count)

        Logic.fillList(boxes)
        currentFrame = Logic.lastFrame()
        Logic.printList()

        if self.state==dd_enum.drive:
            if currentFrame.stop_executed:
                self.stop_action()

            if currentFrame.aqua_executed:
                self.aqua_action()

            if currentFrame.person:
                self.state = dd_enum.person
                self.stop_lanefollow()
                print('Person detected and stop')

            if currentFrame.thirty_start_executed:
                self.drive_action_30()

            if currentFrame.thirty_end_executed:
                self.thirty_end_action()

        elif self.state == dd_enum.stopp:
            if self.count == 1:
                self.state = dd_enum.drive
                self.drive_action()
            else:
                self.count = self.count-1

        elif self.state == dd_enum.aqua:
            self.state = dd_enum.drive
            self.drive_action()

        elif self.state == dd_enum.person:
            if currentFrame.person_out:
                self.person_action_out()

    def person_action_out(self):
        self.state = dd_enum.drive
        self.start_lanefollow()
        print('no Person detected start lane follow')

    def drive_action(self):
        self.start_lanefollow()
        print('start lane follow')
        self.yellow_led.off()
        self.orange_led.off()

    def stop_action(self):
        self.state = dd_enum.stopp
        self.stop_lanefollow()
        self.count = 20
        print('Stop detected and stopped')
        self.yellow_led.on()

    def aqua_action(self):
        self.state = dd_enum.aqua
        self.stop_lanefollow()
        #self.rotation()
        self.back_and_forth()
        print('Aqua detected and do rotation')
        self.orange_led.on()

    def drive_action_30(self):
        velo = velocity_thirty_zone
        msg = Float64(velo)
        self.velo_pub.publish(msg)
        print('slow speed')
        self.red_led.on()

    def thirty_end_action(self):
        velo = velocity_default
        msg = Float64(velo)
        self.velo_pub.publish(msg)
        print('normal speed')
        self.red_led.off()

if __name__ == '__main__':
    try:
        dd_inference_autonomous_drive_node("deepdrive")
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
    except KeyboardInterrupt:
        msg = Joy(header=None, axes=stop_vector, buttons=buttons_vector)



#     lboxes = [
#         [dd_obj_msg(xmin=200, xmax=300, ymin=10, ymax=200, class_="stop", probability=0.97),
#               dd_obj_msg(xmin=200, xmax=300, ymin=10, ymax=200, class_="stop", probability=0.96)],
#         [],[],[],[],
#         [dd_obj_msg(xmin=200, xmax=300, ymin=10, ymax=200, class_="aqua", probability=0.97),
#          dd_obj_msg(xmin=100, xmax=200, ymin=10, ymax=20, class_="aqua", probability=0.6)]
#         ]
#     count = 0
#     for image in lboxes:
# #        print(image)
#         decision_logic(image)

