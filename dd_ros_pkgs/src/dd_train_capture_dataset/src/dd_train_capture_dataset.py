#!/usr/bin/env python

import roslib
import rospy
from sensor_msgs.msg import CompressedImage

import numpy as np
import os
import cv2
from datetime import datetime

class generate_dataset:
    
    def __init__(self, name, output_dir="images/"):
        # Create directory
        if not os.path.exists(output_dir):
            os.mkdir(output_dir)
        self.output_dir = output_dir

        self.name = name
        self.image_cnt = 0
        self.take_image_for_dataset()

    def take_image_for_dataset(self):
        sub = rospy.Subscriber("/" + self.name + "/camera_node/image/compressed",
                               CompressedImage, self.save_received_image,  queue_size = 1)
        
    def save_received_image(self, ros_image):
        
        now = datetime.now()
        filename = self.output_dir + "/" + now.strftime("image_%Y_%m_%d_%H_%M_%S_%f.jpg")
        image = np.fromstring(ros_image.data, np.uint8)
        image = cv2.imdecode(image, cv2.IMREAD_COLOR)
        #filename = "%s/image_%06d.jpg" % (self.output_dir, self.image_cnt)
        cv2.imwrite(filename,image)
        print ("Saved %s" % filename)
        self.image_cnt += 1
  

generate_dataset("deepdrive")

rospy.init_node('generate_dataset', anonymous=True)
try:
    rospy.spin()
except KeyboardInterrupt:
    cv2.destroyAllWindows()
