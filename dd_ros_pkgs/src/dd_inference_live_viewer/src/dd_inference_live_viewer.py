#!/usr/bin/env python

import cv2
import time
import numpy as np
import logging

import rospy
from dd_inference_autonomous_drive.msg import dd_frame_predicted_msg
classes_dict =  {'1': ['stop', [0,0,255]],
                 '2': ['aqua', [255,0,0]],
                 '3': ['30 start', [210,182,247]],
                 '4': ['30 end', [0,0,0]],
                 '5': ['person', [25,211,255]] }

class dd_inference_live_viewer:
    def __init__(self, name):
        logging.info("dd_inference_live_viewer started")
        self.startup_image = cv2.imread("startup_image.jpg")
        self.crt_img = self.startup_image
        self.name = name
        sub = rospy.Subscriber("/" + self.name + "/object_detection/compressed_image_with_boxes",
                               dd_frame_predicted_msg, self.update_image,   queue_size = 1)

    def add_box(self, box, bgr_color=(255, 255, 255), thickness=2):
        box[0] = np.clip(box[0], 0, self.crt_img.shape[1])
        box[1] = np.clip(box[1], 0, self.crt_img.shape[0])
        box[2] = np.clip(box[2], 0, self.crt_img.shape[1])
        box[3] = np.clip(box[3], 0, self.crt_img.shape[0])
        
        self.crt_img = cv2.rectangle(self.crt_img, tuple(box[0:2]), tuple(box[2:4]), bgr_color, thickness)

    def add_text(self, text, position):
        position[0] = np.clip(position[0], 0, self.crt_img.shape[1])
        position[1] = np.clip(position[1], 0, self.crt_img.shape[0])
        
        cv2.putText(img=self.crt_img, text=text, fontFace=cv2.FONT_HERSHEY_TRIPLEX, fontScale=0.4, org=tuple(position), color=(255, 255, 255))
    def add_textbox(self, text, box, bgr_color):
        self.add_box(box, bgr_color=bgr_color)
        self.add_box([box[0], box[1], box[0]+int(7.5*len(text)), box[1]-15], bgr_color=bgr_color, thickness=-1)
        self.add_text(text, [box[0],box[1]-2])

    def update_image(self, img_with_boxes):
        logging.info("image with boxes received")
        print("image with boxes received")
        # Decompress image
        image = img_with_boxes.frame.data
        if len(image) > 0:
            image = np.fromstring(image, np.uint8)
            image = cv2.imdecode(image, cv2.IMREAD_COLOR)
            self.crt_img = image
        else:
            #No connection image
            self.crt_img = self.startup_image      

        # Draw boxes
        logging.info(str(len(img_with_boxes.boxes)) + " boxes recognized")
        print(str(len(img_with_boxes.boxes)) + " boxes recognized")
        for box in img_with_boxes.boxes:
            logging.info(box)
            bbox = [box.xmin, box.ymin, box.xmax, box.ymax ]
            #for box.class_ in classes_dict.keys():
            class_name=classes_dict[box.class_][0]
            color = classes_dict[box.class_][1]


            self.add_textbox("%s %.2f" % (class_name, box.probability), bbox, color)

        print("before imshow")
        cv2.imshow("DeepDrive Image", self.crt_img)
        print("after imshow")
        cv2.waitKey(10)
        print("update image finished")
        logging.info("update image finished")

FORMAT = '%(asctime)-15s -8s %(message)s'
logging.basicConfig(format=FORMAT, level=logging.INFO)

dd_inference_live_viewer("deepdrive")

rospy.init_node('deepdrive_live_viewer', anonymous=True)
try:
    rospy.spin()
except KeyboardInterrupt:
    cv2.destroyAllWindows()

