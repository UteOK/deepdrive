[Watch Demo on YouTube](https://youtu.be/3JXph02AQ5k) 

[![IMAGE ALT TEXT HERE](https://img.youtube.com/vi/3JXph02AQ5k/0.jpg)](https://www.youtube.com/watch?v=3JXph02AQ5k)


VERSION 1.0


HDM Stuttgart

Medieninformatik


Authorin       :  Ute Orner-Klaiber

Betreuer       :  Prof. Johannes Maucher, Johannes Theodoridis, Daniel Grießhaber 